CXX = g++
LINK = ld
CPPFLAGS = -Wall -Wextra -std=c++17 -O3 -fPIC -shared
PREFIX=/usr/local

all : libgetoptcpp.so

win : CXX = x86_64-w64-mingw32-g++
win : CPPFLAGS = -Wall -Wextra -std=c++17 -O3 -fPIC -shared -DWIN
win : getoptcpp.dll
win : LINK = x86_64-w64-mingw32-ld

libgetoptcpp.so: getoptcpp.o
	$(LINK) -shared $^ $(LDLIBGS) -o $@

getoptcpp.dll: getoptcpp.o
	$(CXX)  $(CPPFLAGS) -Wl,--out-implib,lib$(@).a $(LDLIBGS) -o $@ $^

.PHONY: install all clean
install: libgetoptcpp.so
	mkdir -p $(DESTDIR)$(PREFIX)/lib
	cp $< $(DESTDIR)$(PREFIX)/lib/$<

clean :
	rm getoptcpp.o
