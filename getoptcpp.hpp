#pragma once
#include <unordered_map>
#include <vector>
#include <string>
#include <getopt.h>

#ifdef WIN
#define DLL __declspec(dllexport)
#else
#define DLL
#endif

class DLL argParser{
    public:
    argParser(int _argc, char** _argv, bool _evalAlways = false);
    void addOptionLong(const std::string,bool arg = false,char s = 0, std::string help = "");
    bool isSet(std::string);
    std::string get(std::string);
    void set(std::string,std::string val = "");
    void eval();
    std::string getHelp();
    private:
    struct optionW{
        option o;
        bool arg;
        std::string s = "";
        std::string help ="";
    };
    std::unordered_map<std::string,std::string> values; //Only longest name
    std::vector<optionW> keys;
    int argc;
    char **argv;
    bool evalAlways;
    std::string shortopt = "";
};
