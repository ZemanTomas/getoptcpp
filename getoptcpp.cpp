#include "getoptcpp.hpp"
#include <cstring>

using namespace std;

argParser::argParser(int _argc, char** _argv, bool _evalAlways):
    argc(_argc),
    argv(_argv),
    evalAlways(_evalAlways)
{}
void argParser::addOptionLong(const string _name,bool arg,char s, string help){
    string name(_name);
    char *namec = (char*)malloc((name.size()+1)*sizeof(char));
    strcpy(namec,name.c_str());
    optionW tmp = {{namec,arg,0,s},arg,move(name),help};
    keys.push_back(tmp);
    string str_arg = "",str_opt = "";
    if(s!=0){str_opt+=s;if(arg!=0){str_arg=":";}shortopt+=str_opt+str_arg;}
    values[_name]="";
    if(evalAlways){eval();}
}
string argParser::getHelp(){
    return "";
}
bool argParser::isSet(string name){
    return values.find(name) != values.end();
}
string argParser::get(string name){
    return isSet(name)?values[name]:"";
}
void argParser::set(string name,string val){
    if(isSet(name)){values[name]=val;}
}
void argParser::eval(){
    values.clear();
    int c;
    int digit_optind = 0;
    option *long_options = new option[keys.size()+1];
    for(optionW op : keys){
        long_options[digit_optind++] = op.o;
    }
    long_options[digit_optind] = {0,0,0,0};
    digit_optind = 0;

   while (1) {
        int option_index = 0;

       c = getopt_long(argc, argv, shortopt.c_str(),
                 long_options, &option_index);
        if (c == -1){break;}
        for(optionW op : keys){
            if(op.o.val == c){
                if(op.arg){
                    values[op.s]=optarg;
                }else{
                    values[op.s]="";
                }
                break;
            }
        }
    }
    delete[] long_options;
}
